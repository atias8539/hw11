#include "threads.h"
#include <iostream>
#include <thread>
#define TWO 2
void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}
void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();

}
void printVector(vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		std::cout << primes[i] << std::endl;
	}
}
bool checkPrime(int number)
{
	bool isPrime = true;
	for (int i = TWO; i <= number / TWO; ++i)
	{
		if (number % i == 0)
		{
			isPrime = false;
		}
	}
	return isPrime;
}
void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i < end; i++)
	{
		if (checkPrime(i))
		{
			primes.push_back(i);
		}
	}
}
vector<int> callGetPrimes(int begin, int end)
{
	clock_t tStart = clock();

	vector <int> primes;
	std::thread t1(getPrimes, begin, end, ref(primes));
	t1.join();
	std::cout << "Time taken:" << (double)(clock() - tStart) << std::endl;
	return primes;

}

void writePrimesToFile(int begin, int end, ofstream& file)
{

	for (int i = begin; i < end; i++)
	{
		if (checkPrime(i))
		{
			file << i << std::endl;

		}
	}

}
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{


	ofstream myfile;
	myfile.open(filePath);
	int turn = end / N;
	clock_t tStart = clock();
	int sts = end - begin;
	sts = sts / N;
	for (int i = 0; i < N; i++)
	{
		std::thread t1(writePrimesToFile, sts*i, sts*(i + 1), ref(myfile));
		t1.join();
	}
	std::cout << "Time taken:" << (double)(clock() - tStart) << std::endl;

	myfile.close();
}
